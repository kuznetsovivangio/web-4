<?php
  header('Content-Type: text/html; charset=UTF-8');
  setlocale(LC_ALL, 'Russian_Russia.65001');

  if (!empty($_COOKIE["complete"]) && $_COOKIE["complete"]==true && $_COOKIE["refresh"]==0) {
    echo "Запрос был успешно отправлен!";
    setcookie("complete", false);
  } else {
    if (empty($_COOKIE["name"])) echo "Поле с именем не заполнено.<br/>";
    if (empty($_COOKIE["email"])) echo "Поле с электронной почтой не заполнено.<br/>";
    if (empty($_COOKIE["birthyear"])) echo "Поле с датой рождения не заполнено.<br/>";
    if (empty($_COOKIE["sex"])) echo "Поле с полом не заполнено.<br/>";
    if (empty($_COOKIE["limb"])) echo "Поле с количеством конечностей не заполнено.<br/>";
    if (empty($_COOKIE["superpowers"])) echo "Поле с суперспособностями не заполнено.<br/>";
    if (empty($_COOKIE["confirm"])) echo "Вы должны ознакомиться с контрактом.<br/>";
  }
  if (isset($_COOKIE["refresh"])) if ($_COOKIE["refresh"]==1) {
    setcookie("refresh", 0);
    header("Refresh: 0");
  }
  

  if ($_SERVER['REQUEST_METHOD'] == 'POST')
  try{
    setcookie("refresh", 1);
    $check = true;
    if (!empty($_POST['name'])) {
      setcookie("name", $_POST["name"], time()+31536000);
      if (preg_match("/^[A-Z]{1}[a-z]{0,}$/", $_POST['name'])==0) {
        setcookie("wrong_name", true, time()+31536000);
        $check = false;
      } else {
        if (isset($_COOKIE["wrong_name"])) setcookie("wrong_name", false, time()-1);
      }
    } else {
      setcookie("name", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['email'])) {
      setcookie("email", $_POST["email"], time()+31536000);
      if (preg_match("/^[\-_A-Za-z0-9]{1,}@[A-Za-z0-9]{1,}.[A-Za-z]{2,}$/", $_POST['email'])==0) {
        setcookie("wrong_email", true, time()+31536000);
        $check = false;
      } else {
        if (isset($_COOKIE["wrong_email"])) setcookie("wrong_email", false, time()-1);
      }
    } else {
      setcookie("email", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['birthyear'])) {
      setcookie("birthyear", $_POST["birthyear"], time()+31536000);
    } else {
      setcookie("birthyear", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['sex'])) {
      setcookie("sex", $_POST["sex"], time()+31536000);
    } else {
      setcookie("sex", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['limb'])) {
      setcookie("limb", $_POST["limb"], time()+31536000);
    } else {
      setcookie("limb", "", time()-1);
      $check = false;
    }
    if (isset($_COOKIE["superpowers"]))
        for ($i = 0; $i<count($_COOKIE["superpowers"]); $i++) {
          setcookie("superpowers[$i]", "", time()-1);
        }
    if (!empty($_POST['superpowers'])) {
      for ($i = 0; $i<count($_POST["superpowers"]); $i++) {
        setcookie("superpowers[$i]", $_POST["superpowers"][$i], time()+31536000);
      }
    } else {
      $check = false;
    }
    if (!empty($_POST['biography'])) {
      setcookie("biography", $_POST["biography"], time()+31536000);
    } else {
      setcookie("biography", "", time()-1);
      $check = false;
    }
    if (empty($_POST['confirm'])) {
      $check = false;
    }

    if ($check==true) {
      setcookie("name", "", time()-1);
      setcookie("email", "", time()-1);
      setcookie("birthyear", "", time()-1);
      setcookie("sex", "", time()-1);
      setcookie("limb", "", time()-1);
      if (isset($_COOKIE["superpowers"])) for ($i = 0; $i<count($_COOKIE["superpowers"]); $i++) {
        setcookie("superpowers[$i]", "", time()-1);
      }
      setcookie("biography", "", time()-1);
    }
    
    if ($check==false) {
      header("Refresh: 0");
      exit;
    }
    
    $db_host = 'localhost';
    $db_user = 'u20967';
    $db_password = '7306510';

    $bd = new PDO("mysql:host=$db_host; dbname=$db_user", $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));

    $query = $bd->prepare("INSERT INTO query SET name = ?, email = ?, birthyear = ?, sex = ?, limb = ?, biography = ?");
    $query -> execute([$_POST['name'], $_POST['email'], $_POST['birthyear'], $_POST['sex'], $_POST['limb'], $_POST['biography']]);
    $id_query = $bd->lastInsertId();

    $list = implode(', ',$_POST['superpowers']);
    $superpowers = $bd->prepare("INSERT INTO superpowers SET list = ?");
    $superpowers -> execute([$list]);
    $id_superpowers = $bd->lastInsertId();

    $superheroes = $bd->prepare("INSERT INTO superheroes SET id_hero = ?, id_superpowers = ?");
    $superheroes -> execute([$id_query, $id_superpowers]);

    header("Refresh: 0");
    setcookie("complete", true);
  } catch(PDOException $e){
    echo "Произошла ошибка при отправке запроса!";
    print('Error : ' . $e->getMessage());
  }
?>
